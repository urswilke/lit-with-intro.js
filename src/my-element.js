import { LitElement, css, html } from "lit";
import "./subElement.js";
import { default as introJs } from "intro.js";

export class MyElement extends LitElement {
  async firstUpdated() {
    // https://stackoverflow.com/questions/58035998/run-a-function-once-all-children-element-are-actually-updated/58125954#58125954
    const children = this.renderRoot.querySelectorAll("*");
    await Promise.all(Array.from(children).map((c) => c.updateComplete));

    const sub_el = Array.from(children).filter(
      (x) => x.tagName === "SUB-ELEMENT"
    )[0];
    introJs()
      .setOptions({
        steps: [
          {
            title: "Welcome",
            intro:
              "Hello! 👋<br>This demonstrates how to use intro.js with nested lit elements",
          },
          {
            element: this.renderRoot.querySelector("#in-parent-el"),
            intro: "This step focuses on a div in the parent lit element",
          },
          {
            element: sub_el.renderRoot.querySelector("#in-child-el"),
            intro: "This step focuses on a div in a nested child lit element",
          },
        ],
      })
      .start();
  }

  render() {
    return html`
      <div id="in-parent-el">This is a div inside the parent lit element</div>
      <sub-element></sub-element>
    `;
  }
  static get styles() {
    return css`
      div {
        margin: 20px;
        border: 3px red solid;
        text-align: center;
      }
    `;
  }
}

window.customElements.define("my-element", MyElement);
