import { LitElement, css, html } from "lit";
export class SubElement extends LitElement {
  render() {
    return html` <div id="in-child-el">A div in the nested child lit element</div> `;
  }
  static get styles() {
    return css`
      div {
        margin: 20px;
        border: 3px blue dotted;
        text-align: center;
      }
    `;
  }
}

window.customElements.define("sub-element", SubElement);
