# Example repo how to use intro.js with nested lit elements

Deployed [here](https://lit-with-intro-js-urswilke-1e2097115df7650f254bc0dc936b8aed4a95.gitlab.io/).

In order to run this on your machine, run:

```
git clone https://gitlab.com/urswilke/lit-with-intro.js
cd lit-with-intro.js/
npm i
npm run dev
```
